#!/usr/bin/env python3

from z3 import *

print("Using Z3 version: %s" % z3.get_version_string())

s = Solver()

print(s.check())
print(s.model())
